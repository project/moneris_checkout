(function ($, Drupal) {
  Drupal.behaviors.monerisCheckout = {
    attach: function (context, settings) {
      var myCheckout = new monerisCheckout();

      myCheckout.setMode(settings.moneris_checkout.mode);
      myCheckout.setCheckoutDiv(settings.moneris_checkout.div);

      myCheckout.setCallback("page_loaded", myPageLoad);
      myCheckout.setCallback("cancel_transaction", myCancelTransaction);
      myCheckout.setCallback("error_event", myErrorEvent);
      myCheckout.setCallback("payment_receipt", myPaymentReceipt);
      myCheckout.setCallback("payment_complete", myPaymentComplete);

      if (settings.moneris_checkout.ticket) {
        myCheckout.startCheckout(settings.moneris_checkout.ticket);
      }

      function myPageLoad(event) {
        console.log('Page load callback.');
        console.log(event);
      }

      function myCancelTransaction(event) {
        console.log('Cancel transaction callback.');
      }

      function myErrorEvent(event) {
        console.log('Error event callback.');
      }

      function myPaymentReceipt(event) {
        console.log('Payment receipt callback.');
      }

      function myPaymentComplete(event) {
        console.log('Payment complete callback.');
      }
    }
  };
})(jQuery, Drupal);