# Moneris Checkout (MCO)

Basic module to deliver functionality to display MCO and perform 
checkout process.

## Requirements

TODO

* API Token
* Store Id
* Etc...

## Depedencies

TODO

## Documentation

https://developer.moneris.com:3000/livedemo/checkout/overview/guide/php

## Usage/Setup

Enable module like any other Drupal module.
