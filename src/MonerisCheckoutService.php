<?php

namespace Drupal\moneris_checkout;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * MonerisCheckoutService service.
 */
class MonerisCheckoutService {

  /**
   * QA Gateway URL
   */
  const MONERIS_QA_GATEWAY_URL = 'https://gatewayt.moneris.com';

  /**
   * PROD Gateway URL
   */
  const MONERIS_PROD_GATEWAY_URL = 'https://gateway.moneris.com';

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a MonerisCheckoutService object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   */
  public function __construct(RequestStack $request_stack, ClientInterface $http_client) {
    $this->requestStack = $request_stack;
    $this->httpClient = $http_client;
  }



  /**
   * Preload
   */
  public function checkoutPreload() {
    $url = 'https://gatewayt.moneris.com/chkt/request/request.php';
    $body = [
      'store_id' => '',
      'api_token' => '',
      'checkout_id' => '',
      'txn_total' => '',
      'environment' => '',
      'action' => 'preload',
      'language' => 'en',
    ];
    
    try {
      $response = $this->httpClient->post($url, [
        'body' => json_encode($body),
      ]);
      $response_data = json_decode($response->getBody()->getContents(), TRUE);
    }
    catch (RequestException $e) {
      watchdog_exception('moneris_checkout', $e);
    }

    return $response_data;
  }

}
