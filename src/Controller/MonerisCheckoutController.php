<?php

namespace Drupal\moneris_checkout\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\moneris_checkout\MonerisCheckoutService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;


class MonerisCheckoutController extends ControllerBase
{

  /**
   * Moneris checkout service.
   *
   * @var \Drupal\moneris_checkout\MonerisCheckoutService
   */
  protected $monerisCheckoutService;

  /**
   * Constructor.
   *
   * @param \Drupal\moneris_checkout\MonerisCheckoutService $moneris_checkout
   *   The moneris checkout service.
   */
  public function __construct(MonerisCheckoutService $moneris_checkout)
  {
    $this->monerisCheckoutService = $moneris_checkout;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container)
  {
    return new static (
      $container->get('moneris_checkout.service')
    );
  }

  /**
   * Checkout.
   */
  public function checkout()
  {
    $preloadResponse = $this->monerisCheckoutService->checkoutPreload();
    $ticket = $preloadResponse['response']['ticket'] ?? '';

    $build = [
      '#theme' => 'moneris_checkout',
    ];

    $build['#attached']['library'][] = 'moneris_checkout/moneris_checkout';
    $build['#attached']['drupalSettings']['moneris_checkout'] = [
      'ticket' => $ticket,
      'mode' => 'qa',
      'div' => 'monerisCheckout',
    ];

    return $build;
  }
}