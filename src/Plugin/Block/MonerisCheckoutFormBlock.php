<?php

namespace Drupal\moneris_checkout\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\moneris_checkout\MonerisCheckoutService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a moneris checkout form block.
 *
 * @Block(
 *   id = "moneris_checkout_form_block",
 *   admin_label = @Translation("Moneris checkout form"),
 *   category = @Translation("Moneris Checkout")
 * )
 */
class MonerisCheckoutFormBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The moneris_checkout.service service.
   *
   * @var \Drupal\moneris_checkout\MonerisCheckoutService
   */
  protected $monerisCheckoutService;

  /**
   * Constructs a new MonerisCheckoutFormBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\moneris_checkout\MonerisCheckoutService $moneris_checkout_service
   *   The moneris_checkout.service service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MonerisCheckoutService $moneris_checkout_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->monerisCheckoutService = $moneris_checkout_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('moneris_checkout.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'foo' => $this->t('Hello world!'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['foo'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Foo'),
      '#default_value' => $this->configuration['foo'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['foo'] = $form_state->getValue('foo');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $preloadResponse = $this->monerisCheckoutService->checkoutPreload();
    $ticket = $preloadResponse['response']['ticket'] ?? '';

    $build = [
      '#theme' => 'moneris_checkout',
      '#height' => '642',
    ];

    $build['#attached']['library'][] = 'moneris_checkout/moneris_checkout';
    $build['#attached']['drupalSettings']['moneris_checkout'] = [
      'ticket' => $ticket,
      'mode' => 'qa',
      'div' => 'monerisCheckout',
    ];

    return $build;
  }

}
